## About Project

TASK

1.	Design and create  a database to hold student details, details must include: Name and Surname, Date of Birth, ID Number, Address, Level and Class
2.	Create a screen to display the list
3.	Make the functions to add, edit and delete a student.

Extra Notes:
•	No Authentication is required
•	Validation is required

Database: MySQL
Programming Language: PHP

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
