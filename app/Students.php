<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
	protected $fillable = [
        'student_name',
        'student_surname',
        'student_date_of_birth',
        'student_address',
        'student_id_number'
    ];
	public function career(){
		return $this->hasMany('App\Careers')
			->orderBy('careers.start', 'desc')
			->with('details');
	}

	public function lastStudentCareer(){
		return $this->hasOne('App\Careers')
			->orderBy('careers.start', 'desc')
			->limit(1)
			->with('details');

	}

}
