<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Students;

class HomeController extends Controller
{
    use ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
        // $aStudents = Students::with('career')->get();
        $aStudents = Students::all();
        $oList = Students::with('career')->get();
        //dd($oList->toArray());
        return view('home', [
            'aStudents' => $aStudents->toArray(),
            'aList' => $oList->toArray()
        ]);
    }

    /**
     * Show the Create form.
     *
     */
    public function createStudent()
    {
        return view('studentCreateEdit', [
            'action' => 'Create',
            'aStudent' => new Students,
            'aCareer' => []
        ]);
    }

    /**
     * Show the Edit form.
     *
     */
    public function editStudent(Request $request)
    {
        $Student = Students::find($request->id);

        if(!$Student)
            abort('400', 'Bad Request');


        return view('studentCreateEdit', [
            'action' => 'Edit',
            'aStudent' => $Student,
            'aCareer' => []
        ]);
    }

    public function deleteStudent(Request $request)
    {
        $Student = Students::find($request->id);

        if(!$Student)
            return response()->json(['status' => 'fail']);

        $Student->delete();
        return response()->json(['status' => 'success']);
    }



    public function saveStudent(Request $request)
    {

        $rules = [
            'student.student_name' => 'required|max:255|regex:/^[a-zA-Z]+$/u',
            'student.student_surname' => 'required|max:255|regex:/^[a-zA-Z]+$/u',
            'student.student_date_of_birth' => 'required|date_format:Y-m-d|date',
            'student.student_address' => 'required|max:255',
            'student.student_id_number' => 'required|unique:students,student_id_number,'.$request->student["id"],
        ];


        $this->validate($request, $rules);

        if( !$request->student["id"] )
            $oStudent = new Students;
        else
            $oStudent = Students::find($request->student["id"]);

        if( $oStudent ){
            $oStudent->fill($request->student);
            $oStudent->save();

            return response()->json([
                'status' => 'success',
                'student_id' => $oStudent->id
            ]);
        }
        return response()->json([
            'status' => 'fail'
        ]);
    }
}
