<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    public function type()
    {
    	return $this->belongsTo('App\ClassTypes', 'class_types_id');
    }

    public function institute()
    {
    	return $this->belongsTo('App\Institutes', 'institutes_id');
    }
	public function level(){
		return $this->belongsTo('App\Levels', 'levels_id');
	}
}
