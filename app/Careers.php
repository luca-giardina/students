<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Careers extends Model
{
  public function student(){
		return $this->belongsTo('App\Students', 'students_id');
	}
  public function class(){
		return $this->belongsTo('App\Classes', 'classes_id');
	}

  public function details()
  {
    return $this->belongsTo('App\Classes', 'classes_id')->with('institute', 'type', 'level');
  }

}
