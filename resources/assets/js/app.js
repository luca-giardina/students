
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));

//const app = new Vue({
//    el: '#app'
//});

$(document).ready(function(){
	$("input[type=date]").datepicker({
		dateFormat: "yy-mm-dd"
	});
});


$(".last-career").on("click", function(){ 
	rel = $(this).attr("rel");
	$('tr.past-career').each(function() {
		if( $(this).attr('rel') == rel )
			$(this).toggleClass('hidden');
	}); 
})

$("#EditStudent").on('click', function(event){
	event.preventDefault();
	id = $('select[name=student]').val();
	if( id )
		window.location.href = "/student/edit/" + $('select[name=student]').val();
});

$("#AddStudent").on('click', function(){
	window.location.href = "/student/new";
});


$("#DeleteStudent").on('click', function(event){

	event.preventDefault();

	selected = $('select[name=student]');

	if( selected.val() )
	{
		text = 'Want you to delete ' + selected.find(":selected").text() + '?';
		if( confirm(text) )
		{
			$.ajax({
				'url': '/student/delete',
				'type': 'POST',
				'dataType': 'JSON',
				'data': {
					'id': selected.val(),
					'_token': Laravel.csrfToken
				}
			})
			.done(function(data) {
				console.log( data );
				if( data.status = 'success')
				{
					alert(selected.find(":selected").text() + ' deleted');
					selected.find(":selected").remove();
				}
			})
			.fail(function() {
				alert( "Error" );
			})
		}
	}
});

$("#saveStudent").on('submit', function(event){

	event.preventDefault();

	var data = {};

	$.each($(this).serializeArray(), function(){
		data[this.name] = this.value;
	});

	$.ajax({
		'url': '/student/save',
		'type': 'POST',
		'dataType': 'JSON',
		'data': {
			'student': data,
			'_token': Laravel.csrfToken
		}
	})
	.done(function(result) {
		console.log( result );
		if(result.status == 'success'){
			alert("success");
			if( data.id == '' )
				window.location.href = "/student/edit/" + result.student_id;
		}
		else
			alert("fail");

	})
	.fail(function(data) {
		result = data.responseText;
		var err = eval("(" + result + ")");

		text = '';
		$.each(err, function(i,v){
			text += v + '\n';
		});

		alert(text);
	})
});