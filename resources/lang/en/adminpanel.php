<?php

return [

	/*
	|--------------------------------------------------------------------------
	| User Panel Language Lines
	|--------------------------------------------------------------------------
	|
	| Questo file è relativo a tutta la parte visibile all'utente
	| non amministratore
	|
	*/

	'student' => 'Student',
	'id' => 'ID',
	'name' => 'Name',
	'surname' => 'Surname',
	'date_of_birth' => 'Date of birth',
	'id_number' => 'ID Number',
	'address' => 'Address',
	'level' => 'Level',
	'institute' => 'Institute',
	'class' => 'Class',
	'null' => '-',
	'on_going' => 'Completed',
	'yes' => 'Yes',
	'no' => 'No',
];
