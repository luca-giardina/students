<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-2">
        <select name="student" class="form-control custom">
            <option value="" selected disabled="">Select a Student</option>}
            @foreach($aStudents as $aStudent)
                <option value="{{ $aStudent['id'] }}">{{ $aStudent['student_name'] }} {{ $aStudent['student_surname'] }}</option>
            @endforeach
        </select>
    </div>
   <div class="col-md-2">
        <button id="EditStudent" class="btn btn-warning btn-block">
            <span class="glyphicon glyphicon-chevron-left"></span>
            Edit Student
            <span class="glyphicon glyphicon-chevron-right"></span>
        </button>
    </div>
    <div class="col-md-2">
        <button id="DeleteStudent" class="btn btn-danger btn-block">
            <span class="glyphicon glyphicon-chevron-left"></span>
            Delete Student
            <span class="glyphicon glyphicon-chevron-right"></span>
        </button>
    </div>
</div>

<div class="row">
    <div class="md-col-12 padding-tb-20">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <button id="AddStudent" class="btn btn-success btn-block">
                <span class="glyphicon glyphicon-chevron-left"></span>
                Add Student
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>
        </div>
    </div>
</div>