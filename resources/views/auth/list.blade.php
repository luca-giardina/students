<table id="student-list" class="table table-hover text-center">
	<thead>
		<tr>
			<th class="text-center col-md-1">{{ trans('adminpanel.id') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.name') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.surname') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.id_number') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.date_of_birth') }}</th>
			<th class="text-center col-md-2">{{ trans('adminpanel.address') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.level') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.institute') }}</th>
			<th class="text-center col-md-2">{{ trans('adminpanel.class') }}</th>
			<th class="text-center col-md-1">{{ trans('adminpanel.on_going') }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($aList as $Student)

			@if( isset( $Student['career'] ) && $Student['career'] )
				
				@foreach( $Student['career'] as $Career)
				
					<tr @if (!$loop->first) class="hidden past-career" @else class="last-career pointer" @endif rel="{{ $Student["id"] }}">
						@if ($loop->first)
							<td>{{ $Student["id"] }}</td>
							<td>{{ $Student["student_name"] }}</td>
							<td>{{ $Student["student_surname"] }}</td>
							<td>{{ $Student["student_id_number"] }}</td>
							<td>{{ $Student["student_date_of_birth"] }}</td>
							<td>{{ $Student["student_address"] }}</td>
						@else
							<td>{{ trans('adminpanel.null') }}</td>
							<td>{{ trans('adminpanel.null') }}</td>
							<td>{{ trans('adminpanel.null') }}</td>
							<td>{{ trans('adminpanel.null') }}</td>
							<td>{{ trans('adminpanel.null') }}</td>
							<td>{{ trans('adminpanel.null') }}</td>
						@endif


						<td>{{ $Student["career"][$loop->index]['details']['level']['level'] }}</td>
						<td>{{ $Student["career"][$loop->index]['details']['institute']['institute_name'] }}</td>
						<td>{{ $Student["career"][$loop->index]['details']['class_name'] }}</td>
						
						<td>
							@if( $Student["career"][$loop->index]['on_going'] )
								{{ trans('adminpanel.no') }}
							@else
								{{ trans('adminpanel.yes') }}
							@endif
						</td>

					</tr>
				@endforeach

			@else

				<td>{{ $Student["id"] }}</td>
				<td>{{ $Student["student_name"] }}</td>
				<td>{{ $Student["student_surname"] }}</td>
				<td>{{ $Student["student_id_number"] }}</td>
				<td>{{ $Student["student_date_of_birth"] }}</td>
				<td>{{ $Student["student_address"] }}</td>
				<td>{{ trans('adminpanel.null') }}</td>
				<td>{{ trans('adminpanel.null') }}</td>
				<td>{{ trans('adminpanel.null') }}</td>
				<td>{{ trans('adminpanel.null') }}</td>

			@endif

		@endforeach
	</tbody>
</table>
