<form id="saveStudent">
	<input type="hidden" name="id" value="{{$aStudent->id}}">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="col-md-6 padding-tb-20">
					{{ trans('adminpanel.name') }}
				</div>
				<div class="col-md-6 padding-tb-20">
					<input class="form-control" name="student_name" type="text" value="{{$aStudent->student_name}}" required>
				</div>
				<div class="col-md-6 padding-tb-20">
					{{ trans('adminpanel.surname') }}
				</div>
				<div class="col-md-6 padding-tb-20">
					<input class="form-control" name="student_surname" type="text" value="{{$aStudent->student_surname}}" required>
				</div>
				<div class="col-md-6 padding-tb-20">
					{{ trans('adminpanel.date_of_birth') }}
				</div>
				<div class="col-md-6 padding-tb-20">
					<input class="form-control" name="student_date_of_birth" type="date" value="{{$aStudent->student_date_of_birth}}" required>
				</div>
				<div class="col-md-6 padding-tb-20">
					{{ trans('adminpanel.address') }}
				</div>
				<div class="col-md-6 padding-tb-20">
					<input class="form-control" name="student_address" type="text" value="{{$aStudent->student_address}}" required>
				</div>
				<div class="col-md-6 padding-tb-20">
					{{ trans('adminpanel.id_number') }}
				</div>
				<div class="col-md-6 padding-tb-20">
					<input class="form-control" name="student_id_number" type="text" value="{{$aStudent->student_id_number}}" required>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<input type="submit" class="btn btn-block btn-success" value="{{ $action }}">
			</div>
	</div>
</form>