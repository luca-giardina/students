<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/* STUDENTS */
Route::get('/student/new', 'HomeController@createStudent')->name('newStudent');

Route::post('/student/delete', 'HomeController@deleteStudent')->name('deleteStudent');
Route::post('/student/save', 'HomeController@saveStudent')->name('saveStudent');

Route::get('/student/edit/{id}', 'HomeController@editStudent')->name('editStudent');