<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Students;
use App\Classes;
use App\Institutes;
use App\ClassTypes;
use App\Levels;
use App\Careers;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /////////////////////
        // TABLE USERS
        /////////////////////

        // Admin insert
        $this->command->info(PHP_EOL . 'Insert Admin'. PHP_EOL .'user: giardin4.luc4@gmail.com'. PHP_EOL .'pass: admin' . PHP_EOL . PHP_EOL);

        DB::table('users')->delete();

        User::create([
                'name' => 'Luca Giardina',
                'email' => 'giardin4.luc4@gmail.com',
                'password' => Hash::make( 'admin' ),
        ]);


        /////////////////////
        // TABLE STUDENTS
        /////////////////////
        $this->command->info('Creating dummy Students');

        DB::table('students')->delete();


        Students::create([
        	'student_name' => 'Jhon',
        	'student_surname' => 'Snow',
        	'student_date_of_birth' => '1980-03-21',
        	'student_address' => 'Winter Street, 54',
        	'student_id_number' => 'AA000AA'
        ]);
        Students::create([
        	'student_name' => 'Frodo',
        	'student_surname' => 'Beggins',
        	'student_date_of_birth' => '1990-01-01',
        	'student_address' => 'Hobbits Street, 54',
        	'student_id_number' => 'BB000BB'
        ]);



        /////////////////////
        // TABLE LEVELS
        /////////////////////
        $this->command->info('Creating Levels');

        DB::table('levels')->delete();
        Levels::create([ 'level' => 'A', 'description' => 'Introductory Level A']);
        Levels::create([ 'level' => 'B', 'description' => 'Introductory Level B']);
        Levels::create([ 'level' => '1', 'description' => 'General Education']);
        Levels::create([ 'level' => '2', 'description' => 'General Education']);
        Levels::create([ 'level' => '3', 'description' => 'General Education']);
        Levels::create([ 'level' => '4', 'description' => 'Matriculation Certificate, Advanced level']);
        Levels::create([ 'level' => '5', 'description' => 'Undergraduate Diploma, Undergraduate Certificate']);
        Levels::create([ 'level' => '6', 'description' => 'Bachelor\'s Degree']);
        Levels::create([ 'level' => '7', 'description' => 'Master\'s Degree']);
        Levels::create([ 'level' => '8', 'description' => 'Doctoral Degree']);


        /////////////////////
        // TABLE INSTITUTES
        /////////////////////
        $this->command->info('Creating dummy Institutes');

        DB::table('institutes')->delete();

        Institutes::create([
        	'institute_name' => 'High School, Malta',
        	'institute_address' => 'High School Street, 322',
        ]);
        Institutes::create([
        	'institute_name' => 'Classical Lyceum',
        	'institute_address' => 'Classical Lyceum Street, 98',
        ]);
		Institutes::create([
        	'institute_name' => 'University of Malta',
        	'institute_address' => 'University Street, 1121',
        ]);




        /////////////////////
        // TABLE CLASS_TYPES
        /////////////////////
        $this->command->info('Creating dummy ClassTypes');

        DB::table('class_types')->delete();
        ClassTypes::create([ 'field' => 'Information Technology']);
        ClassTypes::create([ 'field' => 'Other']);
        ClassTypes::create([ 'field' => 'Engeenering']);
        ClassTypes::create([ 'field' => 'Mathematics']);



        /////////////////////
        // TABLE CLASSES
        /////////////////////
        $this->command->info('Creating dummy Classes');

        DB::table('classes')->delete();

        Classes::create([
            'class_name' => 'Technical IT Diploma',
            'class_types_id' => 1,
            'institutes_id' => 1,
            'levels_id' => 7
        ]);
        Classes::create([
            'class_name' => 'Classical',
            'class_types_id' => 2,
            'institutes_id' => 2,
            'levels_id' => 7
        ]);
        Classes::create([
            'class_name' => 'Information Technology Bachelor\'s Degree',
            'class_types_id' => 1,
            'institutes_id' => 3,
            'levels_id' => 8
        ]);
        Classes::create([
            'class_name' => 'Information Technology Master\'s Degree',
            'class_types_id' => 1,
            'institutes_id' => 3,
            'levels_id' => 9
        ]);
        Classes::create([
            'class_name' => 'Information Technology Doctoral\'s Degree',
            'class_types_id' => 1,
            'institutes_id' => 3,
            'levels_id' => 10
        ]);

        /////////////////////
        // TABLE CAREERS
        /////////////////////
        $this->command->info('Creating dummy Careers');

        DB::table('careers')->delete();
        Careers::create([
        	'students_id' => 1,
        	'classes_id' => 1,
            'start' => '1995-06-01',
            'end' => '2000-06-30'
        ]);
        Careers::create([
        	'students_id' => 1,
        	'classes_id' => 3,
            'start' => '2000-06-01',
            'end' => '2005-06-30'
        ]);
        Careers::create([
            'students_id' => 1,
            'classes_id' => 4,
            'start' => '2005-06-01',
            'end' => '2007-06-30'
        ]);
        Careers::create([
            'students_id' => 1,
            'classes_id' => 5,
            'start' => '2007-06-01',
            'end' => '2010-06-30'
        ]);
        Careers::create([
        	'students_id' => 2,
        	'classes_id' => 2,
            'start' => '1994-06-01',
            'end' => '1999-06-30'
        ]);
        Careers::create([
        	'students_id' => 2,
        	'classes_id' => 3,
        	'on_going' => 1,
            'start' => '1999-06-01',
        ]);

    }
}
