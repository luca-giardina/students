<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class_name');
            $table->integer('class_types_id')->nullable()->unsigned();
            $table->foreign('class_types_id')->references('id')->on('class_types');
            $table->integer('institutes_id')->nullable()->unsigned();
            $table->foreign('institutes_id')->references('id')->on('institutes');
            $table->integer('levels_id')->nullable()->unsigned();
            $table->foreign('levels_id')->references('id')->on('levels');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
